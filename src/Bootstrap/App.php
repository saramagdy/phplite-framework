<?php

namespace Phplite\Bootstrap;
use Phplite\Exceptions\Whoops;
use Phplite\Session\Session;
use Phplite\Http\Server;
use Phplite\Http\Request;
use Phplite\File\File;
use Phplite\Router\Route;

class App{
    /**
     * App constructor
     * @return void
     */
    private function __constructor(){}
    
    /**
     * Run the application
     * @return void
     */
    public static function run(){
        //Register Whoops
        Whoops::handle();

        //start session
       Session::start(); 

       //server 
       Request::handle();

       //require routes
       File::require_directory('routes');
       

       //handle the route
       Route::handle();
       
       
    }

}