<?php

namespace Phplite\Http;

class Server {

    /**
     * Server constructor
     */
    private function __constructor(){}

    /**
     * Check that server has the key
     * @return bool
     */
    public static function has($key){
      return isset($_SERVER[$key]);
    }

    /**
     * Get the value from the server by the given key
     * @param $key
     * @return $value
     */

     public static function get($key){
       return static::has($key) ? $_SERVER[$key] : null;
     }

    /**
     * Server all
     * @return array
     */
    public static function all(){
      return $_SERVER;
    }

    /**
     * Get path info path
     * @return array
     */
    public static function path_info($path){
        return pathinfo($path);
    }


}