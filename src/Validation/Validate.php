<?php

namespace Phplite\Validation;
use Phplite\Session\Session;
use Rakit\Validation\Validator;
class Validate {
    /**
     * Constructor
     */
    private function __constructor() {}

    /**
     * Validate request
     * @param array $rules
     * @param bool $json
     * 
     * @return mixed
     */
    public static function validate(Array $rules , $json) {
        $validator = new Validator;

        $validation = $validator->validate($_POST + $_FILES, $rules);
        
        if ($validation->fails()) {
            // handling errors
            $errors = $validation->errors();
            if($json) {
                return ['errors' => $errors->firstOfAll()];
            } else {
                Session::set('errors', $errors);
                Session::set('old', Request::all());
                return Url::redirect(Url::previous());
            }
        } 
    }

}