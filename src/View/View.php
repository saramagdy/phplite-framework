<?php 

namespace Phplite\View;

use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Phplite\File\File;
use Jenssegers\Blade\Blade;
use Phplite\Session\Session;

class View {
    /**
     * View Constructor
     */
    private function __constructor() {}
    
    /**
     * Render view file
     * 
     * @param string $path
     * @param array $data 
     * @return string
     */
    public static function viewRender($path, $data = []) {
        $path = 'views' . File::ds() . str_replace(['/', '\\', '.'], File::ds(), $path) . '.php';
        if(! File::exist($path)) {
            throw new \Exception("The view file {$path} is not exist");
        }
        ob_start();
        extract($data);

        include File::path($path);
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    /**
     * Render the view files using blade engine
     * @param string $path
     * @param array $data
     * @return string
     */
    public static function bladeRender($path, $data = []) {
        $balde = new Blade(File::path('views'), File::path('storage/cache'));

        echo $balde->make($path, $data)->render();
    }

    /**
     * Render the view files using twig engine
     * @param string $path
     * @param array $data
     * @return string
     */
    // public static function twigRender($path, $data = []) {
    //     $path =  File::ds() . str_replace(['/', '\\', '.'], File::ds(), $path).'.html.twig';
    //     $loader = new FilesystemLoader(File::path('views'));
    //     $twig = new Environment($loader);
    //     // $balde = new Twig(File::path('views'), File::path('storage/cache'));
    //     echo $twig->render($path, $data);
    // }

    /**
     * Render view file
     * 
     * @param string $path
     * @param array $data 
     * @return string
     */
    public static function render($path, $data = []) {
        $errors = Session::flash('errors');
        $old = Session::flash('old');
        $data = array_merge($data, ['errors' => $errors, 'old' => $old]);
        return static::bladeRender($path, $data);
    }
}